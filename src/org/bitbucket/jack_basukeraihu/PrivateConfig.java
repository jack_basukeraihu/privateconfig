package org.bitbucket.jack_basukeraihu;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PrivateConfig extends JavaPlugin implements Listener{

	protected static PrivateConfig instance;

	private static File saveFolder;

	@Override
	public void onEnable() {
	    getServer().getPluginManager().registerEvents(this, this); //イベントを登録
		instance = this; //インスタンス化

		if (saveFolder == null) {
			saveFolder = new File(instance.getDataFolder(), "PlayerData"); //PlayerDataという名のフォルダを登録
			if (!saveFolder.exists() && !saveFolder.isDirectory()) { //フォルダが存在しない場合は作成
				saveFolder.mkdirs();
			}
		}
	}

	public void ConfigCreate(Player p) { //プレイヤーのconfigを作成するメゾット
		File file = new File(saveFolder, p.getUniqueId() + ".yml"); //onEnableで登録したフォルダ内でUUID.ymlを取得
        if (!file.exists()) { //ファイルが存在しない場合は新規作成
    		YamlConfiguration config = YamlConfiguration.loadConfiguration(file); //ymlフォルダをconfigデータに変換
			config.set("name", p.getName());
			if (!config.contains("kill")) {
				config.set("kill", 0);
			}
			try {
				config.save(file); //ファイルをセーブ
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public int ConfigLoad(Player p) { //プレイヤーのconfigからkill数を取得しkill数を返すメゾット
		File file = new File(saveFolder, p.getUniqueId() + ".yml");
		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
		if (!config.contains("kill")) { //万が一configが空っぽの場合は0を返す
			return 0;
		}
		return config.getInt("kill"); //configに保存されているkill数を返す
	}

	public void ConfigSave(Player p, int Kills) { //プレイヤーのconfigに新たなkill数を保存するメゾット
		File file = new File(saveFolder, p.getUniqueId() + ".yml");
		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
		config.set("kill", Kills); //configに新たなkill数を書き加える
		try {
			config.save(file); //configをセーブする
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) { //プレイヤーがログインした時のイベント
		Player p = e.getPlayer();
		ConfigCreate(p); //configを作成するメゾットを呼び出す
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) { //プレイヤーが死んだ時のイベント
		Player p = e.getEntity();
		if (p.getKiller() != null) { //Killerのnullチェック
			Player killer = p.getKiller();
			ConfigSave(killer, ConfigLoad(killer) + 1); //configを保存するメゾットを呼び出して現在のkill数に+1させる
		}
	}
}
